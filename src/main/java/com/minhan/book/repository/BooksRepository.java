package com.minhan.book.repository;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Book;
import org.springframework.stereotype.Repository;

@Repository
public interface BooksRepository extends BaseRepository<Book, Long> {

}
