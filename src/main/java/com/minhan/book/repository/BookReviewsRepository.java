package com.minhan.book.repository;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.BookReview;
import org.springframework.stereotype.Repository;

@Repository
public interface BookReviewsRepository extends BaseRepository<BookReview, Long> {
}
