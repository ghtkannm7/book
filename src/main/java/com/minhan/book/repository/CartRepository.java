package com.minhan.book.repository;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Cart;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface CartRepository extends BaseRepository<Cart, Long> {
    Optional<Cart> findByUser_IdAndIsCheckoutFalse(Long userId);

    List<Cart> findByIsDeletedIn(Collection<Boolean> isDeleted);

}
