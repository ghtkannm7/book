package com.minhan.book.repository;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Author;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorsRepository extends BaseRepository<Author, Long> {
}
