package com.minhan.book.repository;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriesRepository extends BaseRepository<Category, Long> {

}
