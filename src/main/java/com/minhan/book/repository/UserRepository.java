package com.minhan.book.repository;

import com.minhan.book.model.entity.User;
import com.minhan.book.base.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User, Long> {
    Optional<User> findByMail(String mail);
}
