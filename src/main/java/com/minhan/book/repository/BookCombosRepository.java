package com.minhan.book.repository;

import com.minhan.book.model.entity.BookCombos;
import com.minhan.book.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookCombosRepository extends BaseRepository<BookCombos, Long> {
}
