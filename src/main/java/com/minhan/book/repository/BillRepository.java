package com.minhan.book.repository;

import com.minhan.book.model.entity.Bill;
import com.minhan.book.base.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillRepository extends BaseRepository<Bill, Long> {
}
