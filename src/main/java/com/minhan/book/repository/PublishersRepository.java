package com.minhan.book.repository;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Publisher;
import org.springframework.stereotype.Repository;

@Repository
public interface PublishersRepository extends BaseRepository<Publisher, Long> {

}
