package com.minhan.book.repository;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.CartItems;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartItemsRepository extends BaseRepository<CartItems, Long> {
    Optional<CartItems> findByCart_IdAndBook_Id(Long cartId, Long bookId);

    Optional<CartItems> findByCart_IdAndBookCombos_Id(Long cartId, Long comboId);

    Optional<CartItems> findByCart_IdAndId(Long cartId, Long cartItemId);

}
