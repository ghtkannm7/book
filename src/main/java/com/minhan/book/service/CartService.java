package com.minhan.book.service;

import com.minhan.book.base.BaseService;
import com.minhan.book.model.entity.Cart;
import com.minhan.book.model.request.CartRequest;
import com.minhan.book.model.response.CartItemsResponse;
import com.minhan.book.model.response.CartResponse;

public interface CartService extends BaseService<Cart, CartRequest, CartResponse, Long> {
    CartResponse getUserUnpaidCarts(Long userId);

    CartResponse addToCart(CartRequest cartRequest);

    CartItemsResponse deleteCartItem(Long cartId, Long cartItemId);

    void checkCartBeforePayment(Long userId);
}