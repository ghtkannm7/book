package com.minhan.book.service.impl;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Publisher;
import com.minhan.book.model.request.PublishersRequest;
import com.minhan.book.model.response.PublishersResponse;
import com.minhan.book.repository.PublishersRepository;
import com.minhan.book.service.PublishersService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PublishersServiceImpl implements PublishersService {
    private final PublishersRepository publishersRepository;
    private final ModelMapper mapper;


    @Override
    public BaseRepository<Publisher, Long> getRepository() {
        return publishersRepository;
    }

    @Override
    public PublishersResponse mapEntityToResponse(Publisher publisher) {
        return mapper.map(publisher, PublishersResponse.class);
    }

    @Override
    public Publisher mapRequestToEntity(PublishersRequest publishersRequest) {
        return mapper.map(publishersRequest, Publisher.class);
    }

    @Override
    public PublishersResponse create(PublishersRequest request) {
        return mapEntityToResponse(publishersRepository.save(mapRequestToEntity(request)));
    }

    @Override
    public PublishersResponse update(Long id, PublishersRequest request) {
        return mapEntityToResponse(publishersRepository.save(getById(id).setName(request.getName())));
    }

    @Override
    public PublishersResponse delete(Long id) {
        PublishersResponse byId = getResponseById(id);
        publishersRepository.deleteById(id);
        return byId;
    }
}
