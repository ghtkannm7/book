package com.minhan.book.service.impl;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Author;
import com.minhan.book.model.entity.Book;
import com.minhan.book.model.entity.Category;
import com.minhan.book.model.entity.Publisher;
import com.minhan.book.model.error.ResponseException;
import com.minhan.book.model.request.BooksRequest;
import com.minhan.book.model.response.BooksResponse;
import com.minhan.book.repository.BooksRepository;
import com.minhan.book.service.AuthorsService;
import com.minhan.book.service.BooksService;
import com.minhan.book.service.CategoriesService;
import com.minhan.book.service.PublishersService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;
    private final PublishersService publishersService;
    private final AuthorsService authorsService;
    private final CategoriesService categoriesService;
    private final ModelMapper mapper;

    @Override
    public BaseRepository<Book, Long> getRepository() {
        return booksRepository;
    }

    @Override
    public BooksResponse mapEntityToResponse(Book book) {
        return mapper.map(book, BooksResponse.class);
    }

    @Override
    public Book mapRequestToEntity(BooksRequest booksRequest) {
        return mapper.map(booksRequest, Book.class);
    }

    @Override
    @Transactional
    public BooksResponse create(BooksRequest request) {
        Publisher publisher = publishersService.getById(request.getPublisherId());
        List<Author> authors = authorsService.getByIds(request.getAuthorIds());
        List<Category> categories = categoriesService.getByIds(request.getCategoryIds());

        return mapEntityToResponse(
                booksRepository.save(
                        mapRequestToEntity(request)
                                .setPublisher(publisher)
                                .setAuthors(authors)
                                .setCategories(categories)
                )
        );
    }

    @Override
    public BooksResponse update(Long id, BooksRequest request) {
        Book getById = getById(id);
        Publisher publisher = publishersService.getById(request.getPublisherId());
        List<Author> authors = authorsService.getByIds(request.getAuthorIds());
        List<Category> categories = categoriesService.getByIds(request.getCategoryIds());

        return mapEntityToResponse(
                booksRepository.save(
                        getById
                                .setTitle(request.getTitle())
                                .setPrice(request.getPrice())
                                .setQuantity(request.getQuantity())
                                .setBookFormat(request.getBookFormat())
                                .setPublisher(publisher)
                                .setAuthors(authors)
                                .setCategories(categories)
                )
        );
    }

    @Override
    public BooksResponse delete(Long id) {
        BooksResponse byId = getResponseById(id);
        booksRepository.deleteById(id);
        return byId;
    }

    @Override
    public void subQuantity(Long bookId, Integer quantityCI) {
        Book book = getById(bookId);
        int check = checkQuantityBook(book, quantityCI);

        if (check < 0) {
            throw new ResponseException("The quantity of books in stock is less than the quantity you want to purchase");
        } else if (check == 0) {
            book.setIsDeleted(true).setQuantity(book.getQuantity() - quantityCI);
        } else {
            book.setQuantity(book.getQuantity() - quantityCI);
        }

        booksRepository.save(book);
    }

    private int checkQuantityBook(Book book, Integer quantityCI) {
        return book.getQuantity().compareTo(quantityCI);
    }
}
