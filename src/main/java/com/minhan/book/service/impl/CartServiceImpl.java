package com.minhan.book.service.impl;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Book;
import com.minhan.book.model.entity.BookCombos;
import com.minhan.book.model.entity.Cart;
import com.minhan.book.model.entity.CartItems;
import com.minhan.book.model.error.ResponseException;
import com.minhan.book.model.request.CartItemsRequest;
import com.minhan.book.model.request.CartRequest;
import com.minhan.book.model.response.CartItemsResponse;
import com.minhan.book.model.response.CartResponse;
import com.minhan.book.repository.CartItemsRepository;
import com.minhan.book.repository.CartRepository;
import com.minhan.book.service.BookCombosService;
import com.minhan.book.service.BooksService;
import com.minhan.book.service.CartService;
import com.minhan.book.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {
    private final CartRepository cartRepository;
    private final CartItemsRepository cartItemsRepository;
    private final UserService userService;
    private final BooksService booksService;
    private final BookCombosService bookCombosService;
    private final ModelMapper mapper;

    @Override
    public BaseRepository<Cart, Long> getRepository() {
        return cartRepository;
    }

    @Override
    public CartResponse mapEntityToResponse(Cart cart) {
        return mapper.map(cart, CartResponse.class);
    }

    @Override
    public Cart mapRequestToEntity(CartRequest cartRequest) {
        return mapper.map(cartRequest, Cart.class);
    }

    @Override
    public CartResponse create(CartRequest cartRequest) {
        return null;
    }

    @Override
    public CartResponse update(Long id, CartRequest cartRequest) {
        return null;
    }

    @Override
    public CartResponse delete(Long id) {
        CartResponse responseById = getResponseById(id);
        List<Long> cartItemIds = responseById.getCartItems().stream()
                .map(CartItemsResponse::getId)
                .toList();

        cartRepository.deleteById(id);
        cartItemsRepository.deleteAllById(cartItemIds);
        return responseById;
    }

    @Override
    public CartResponse getUserUnpaidCarts(Long userId) {
        return getByUserIdUnpaidCarts(userId)
                .map(this::mapEntityToResponse)
                .orElse(null);
    }

    @Override
    @Transactional
    public CartResponse addToCart(CartRequest cartRequest) {
        CartItemsRequest cartItemRequest = cartRequest.getCartItemId();
        boolean bookIdExist = Objects.nonNull(cartItemRequest.getBookId());

        Book b = bookIdExist
                ? booksService.getById(cartItemRequest.getBookId())
                : null;

        BookCombos bc = bookIdExist
                ? null
                : bookCombosService.getById(cartItemRequest.getBookComboId());

        double price = bookIdExist
                ? cartItemRequest.getQuantity() * b.getPrice()
                : cartItemRequest.getQuantity() * bc.getPrice();

        AtomicReference<Cart> cartAtomicReference = new AtomicReference<>();
        getByUserIdUnpaidCarts(cartRequest.getUserId())
                .ifPresentOrElse(cart -> {
//                    ton tai gio hang
                    Optional<CartItems> optionalCartItems = bookIdExist
                            ? cartItemsRepository.findByCart_IdAndBook_Id(cart.getId(), cartItemRequest.getBookId())
                            : cartItemsRepository.findByCart_IdAndBookCombos_Id(cart.getId(), cartItemRequest.getBookComboId());

                    optionalCartItems.ifPresentOrElse(cartItem -> {
//                                book da co trong gio hang
                                int quantity = cartItem.getQuantity() + cartItemRequest.getQuantity();
                                if (quantity < 0) {
                                    throw new ResponseException("Quantity must be greater than 0");
                                } else if (quantity == 0) {
                                    cartItemsRepository.delete(cartItem);
                                } else {
                                    cartItemsRepository.save(cartItem.setQuantity(quantity));
                                }
                            }, () -> {
//                                book chua co trong gio hang
                                if (cartItemRequest.getQuantity() <= 0) {
                                    throw new ResponseException("Quantity must be greater than 0");
                                }
                                cartItemsRepository.save(
                                        CartItems.builder()
                                                .cart(cart)
                                                .book(b)
                                                .bookCombos(bc)
                                                .quantity(cartItemRequest.getQuantity())
                                                .build()
                                );
                            }
                    );

                    cartAtomicReference.set(cartRepository.save(cart.setPrice(cart.getPrice() + price)));
                }, () -> {
//                    chua co gio hang
                    if (cartItemRequest.getQuantity() <= 0) {
                        throw new ResponseException("Quantity must be greater than 0");
                    }

                    cartAtomicReference.set(
                            cartRepository.save(
                                    Cart.builder()
                                            .price(price)
                                            .user(userService.getById(cartRequest.getUserId()))
                                            .build()
                            )
                    );

                    cartItemsRepository.save(
                            CartItems.builder()
                                    .cart(cartAtomicReference.get())
                                    .book(b)
                                    .bookCombos(bc)
                                    .quantity(cartItemRequest.getQuantity())
                                    .build()
                    );
                });

        return mapEntityToResponse(cartAtomicReference.get());
    }

    @Override
    public CartItemsResponse deleteCartItem(Long cartId, Long cartItemId) {
        Optional<CartItems> optional = cartItemsRepository.findByCart_IdAndId(cartId, cartItemId);
        optional.ifPresent(cartItemsRepository::delete);
        return optional
                .map(cartItems -> mapper.map(cartItems, CartItemsResponse.class))
                .orElse(null);
    }

    @Override
    public void checkCartBeforePayment(Long userId) {
        CartResponse cartResponse = getUserUnpaidCarts(userId);
        if (checkCartItemByUserUnpaidIsEmpty(cartResponse)) {
            throw new ResponseException("Cart Items isn't empty");
        }

        Cart cart = mapper.map(cartResponse, Cart.class);
        cart.getCartItems().forEach(cartItems -> {
            Book book = cartItems.getBook();
            BookCombos bookCombos = cartItems.getBookCombos();
            if (Objects.nonNull(book)) {
                booksService.subQuantity(book.getId(), cartItems.getQuantity());
            } else {
                bookCombosService.subQuantity(bookCombos.getId(), cartItems.getQuantity());
            }
        });

        cartRepository.save(cart.setIsCheckout(true));
    }

    private boolean checkCartItemByUserUnpaidIsEmpty(CartResponse cart) {
        return cart.getCartItems().isEmpty();
    }

    private Optional<Cart> getByUserIdUnpaidCarts(Long userId) {
        return cartRepository.findByUser_IdAndIsCheckoutFalse(userId);
    }
}
