package com.minhan.book.service.impl;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Book;
import com.minhan.book.model.entity.BookCombos;
import com.minhan.book.model.error.ResponseException;
import com.minhan.book.model.request.BookCombosRequest;
import com.minhan.book.model.response.BookCombosResponse;
import com.minhan.book.repository.BookCombosRepository;
import com.minhan.book.service.BookCombosService;
import com.minhan.book.service.BooksService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class BookCombosServiceImpl implements BookCombosService {
    private final BookCombosRepository bookCombosRepository;
    private final BooksService booksService;
    private final ModelMapper mapper;

    @Override
    public BaseRepository<BookCombos, Long> getRepository() {
        return bookCombosRepository;
    }

    @Override
    public BookCombosResponse mapEntityToResponse(BookCombos bookCombos) {
        return mapper.map(bookCombos, BookCombosResponse.class);
    }

    @Override
    public BookCombos mapRequestToEntity(BookCombosRequest bookCombosRequest) {
        return mapper.map(bookCombosRequest, BookCombos.class);
    }

    @Override
    public BookCombosResponse create(BookCombosRequest bookCombosRequest) {
        List<Book> books = booksService.getByIds(bookCombosRequest.getBookIds());
        checkBookDuplicate(books);
        BookCombos bookCombos = bookCombosRepository.save(
                mapRequestToEntity(bookCombosRequest)
                        .setBooks(books)
        );

        BaseRepository<Book, Long> repository = booksService.getRepository();
        for (Book book : books) {
            book.setBookCombos(bookCombos);
        }
        repository.saveAll(books);

        return mapEntityToResponse(bookCombos);
    }

    @Override
    public BookCombosResponse update(Long id, BookCombosRequest bookCombosRequest) {
        BookCombos getById = getById(id);
        List<Book> getBookByIds = getById.getBooks();
        List<Book> bookRequest = booksService.getByIds(bookCombosRequest.getBookIds());
        checkBookDuplicate(bookRequest);

//        xoa BookCombos ma bookCombosRequest khong co
        getBookByIds.stream()
                .filter(b -> !bookRequest.contains(b))
                .toList()
                .forEach(b -> b.setBookCombos(null));


//        add BookCombos
        bookRequest.stream()
                .filter(b -> !getBookByIds.contains(b))
                .toList()
                .forEach(b -> b.setBookCombos(getById));

        BaseRepository<Book, Long> repository = booksService.getRepository();
        repository.saveAll(getBookByIds);
        repository.saveAll(bookRequest);

        return mapEntityToResponse(
                bookCombosRepository.save(
                        getById
                                .setName(bookCombosRequest.getName())
                                .setPrice(bookCombosRequest.getPrice())
                                .setBooks(bookRequest)
                )
        );
    }

    @Override
    public BookCombosResponse delete(Long id) {
        BookCombosResponse byId = getResponseById(id);
        List<Book> books = getById(id).getBooks();

        books.forEach(b -> b.setBookCombos(null));
        booksService.getRepository().saveAll(books);

        bookCombosRepository.deleteById(id);
        return byId;
    }

    private void checkBookDuplicate(List<Book> books) {
        boolean isDuplicate = books.stream().anyMatch(b -> Objects.nonNull(b.getBookCombos()));
        if (isDuplicate) {
            throw new DataIntegrityViolationException("Book Combos is duplicated");
        }
    }

    @Override
    public void subQuantity(Long bookCombosId, Integer quantityCI) {
        BookCombos bookCombos = getById(bookCombosId);
        int check = checkQuantityBook(bookCombos, quantityCI);

        if (check < 0) {
            throw new ResponseException("The quantity of combos book in stock is less than the quantity you want to purchase");
        } else if (check == 0) {
            bookCombos.setIsDeleted(true).setQuantity(bookCombos.getQuantity() - quantityCI);
        } else {
            bookCombos.setQuantity(bookCombos.getQuantity() - quantityCI);
        }

        bookCombosRepository.save(bookCombos);
    }

    private int checkQuantityBook(BookCombos bookCombos, Integer quantityCI) {
        return bookCombos.getQuantity().compareTo(quantityCI);
    }
}
