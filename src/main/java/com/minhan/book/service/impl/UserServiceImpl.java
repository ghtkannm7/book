package com.minhan.book.service.impl;

import com.minhan.book.model.entity.User;
import com.minhan.book.model.request.UserRequest;
import com.minhan.book.model.response.UserResponse;
import com.minhan.book.repository.UserRepository;
import com.minhan.book.base.BaseRepository;
import com.minhan.book.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper mapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return getUserByMail(username);
    }

    @Override
    public User getUserByMail(String mail) {
        return userRepository.findByMail(mail).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    @Override
    public BaseRepository<User, Long> getRepository() {
        return userRepository;
    }

    @Override
    public UserResponse mapEntityToResponse(User user) {
        return mapper.map(user, UserResponse.class);
    }

    @Override
    public User mapRequestToEntity(UserRequest userRequest) {
        return mapper.map(userRequest, User.class);
    }

    @Override
    public UserResponse create(UserRequest userRequest) {
        return null;
    }

    @Override
    public UserResponse update(Long id, UserRequest userRequest) {
        return mapEntityToResponse(
                userRepository.save(
                        getById(id)
                                .setPhone(userRequest.getPhone())
                                .setPassword(passwordEncoder.encode(userRequest.getPassword()))
                )
        );
    }

    @Override
    public UserResponse delete(Long id) {
        UserResponse byId = getResponseById(id);
        userRepository.deleteById(id);
        return byId;
    }
}
