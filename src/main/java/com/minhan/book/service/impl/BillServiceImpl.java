package com.minhan.book.service.impl;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Bill;
import com.minhan.book.model.entity.Cart;
import com.minhan.book.model.request.BillRequest;
import com.minhan.book.model.response.BillResponse;
import com.minhan.book.model.response.CartResponse;
import com.minhan.book.repository.BillRepository;
import com.minhan.book.service.BillService;
import com.minhan.book.service.CartService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class BillServiceImpl implements BillService {
    private final BillRepository billRepository;
    private final CartService cartService;
    private final ModelMapper mapper;

    @Override
    public BaseRepository<Bill, Long> getRepository() {
        return billRepository;
    }

    @Override
    public BillResponse mapEntityToResponse(Bill bill) {
        return mapper.map(bill, BillResponse.class);
    }

    @Override
    public Bill mapRequestToEntity(BillRequest billRequest) {
        return mapper.map(billRequest, Bill.class);
    }

    @Override
    public BillResponse create(BillRequest billRequest) {
        Cart cart = cartService.getById(billRequest.getCartId());
        return mapEntityToResponse(
                billRepository.save(
                        Bill.builder()
                                .cart(cart)
                                .totalPrice(billRequest.getTotalPrice())
                                .orderDate(new Date())
                                .build()
                )
        );
    }

    @Override
    public BillResponse update(Long id, BillRequest billRequest) {
        return null;
    }

    @Override
    public BillResponse delete(Long id) {
        BillResponse byId = getResponseById(id);
        billRepository.deleteById(id);
        return byId;
    }

    @Override
    public BillResponse payAfterReceive(Long userId) {
        CartResponse cartResponse = cartService.getUserUnpaidCarts(userId);
        cartService.checkCartBeforePayment(userId);

//        Gui thong bao, websocket, tinh lai tien bill sau khi tru coupon

        return create(
                BillRequest.builder()
                        .cartId(cartResponse.getId())
                        .totalPrice(cartResponse.getPrice())
                        .build()
        );
    }

    @Override
    public BillResponse paymentByCard(Long userId) {
        return null;
    }

}
