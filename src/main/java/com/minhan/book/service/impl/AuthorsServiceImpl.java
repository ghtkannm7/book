package com.minhan.book.service.impl;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Author;
import com.minhan.book.model.request.AuthorsRequest;
import com.minhan.book.model.response.AuthorsResponse;
import com.minhan.book.repository.AuthorsRepository;
import com.minhan.book.service.AuthorsService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorsServiceImpl implements AuthorsService {
    private final AuthorsRepository authorsRepository;
    private final ModelMapper mapper;

    @Override
    public BaseRepository<Author, Long> getRepository() {
        return authorsRepository;
    }

    @Override
    public AuthorsResponse mapEntityToResponse(Author author) {
        return mapper.map(author, AuthorsResponse.class);
    }

    @Override
    public Author mapRequestToEntity(AuthorsRequest authorsRequest) {
        return mapper.map(authorsRequest, Author.class);
    }

    @Override
    public AuthorsResponse create(AuthorsRequest request) {
        return mapEntityToResponse(authorsRepository.save(mapRequestToEntity(request)));
    }

    @Override
    public AuthorsResponse update(Long id, AuthorsRequest request) {
        return mapEntityToResponse(authorsRepository.save(getById(id).setName(request.getName())));
    }

    @Override
    public AuthorsResponse delete(Long id) {
        AuthorsResponse byId = getResponseById(id);
        authorsRepository.deleteById(id);
        return byId;
    }
}
