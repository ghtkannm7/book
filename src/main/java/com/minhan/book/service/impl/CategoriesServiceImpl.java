package com.minhan.book.service.impl;

import com.minhan.book.base.BaseRepository;
import com.minhan.book.model.entity.Category;
import com.minhan.book.model.request.CategoriesRequest;
import com.minhan.book.model.response.CategoriesResponse;
import com.minhan.book.repository.CategoriesRepository;
import com.minhan.book.service.CategoriesService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoriesServiceImpl implements CategoriesService {
    private final CategoriesRepository categoriesRepository;
    private final ModelMapper mapper;

    @Override
    public BaseRepository<Category, Long> getRepository() {
        return categoriesRepository;
    }

    @Override
    public CategoriesResponse mapEntityToResponse(Category category) {
        return mapper.map(category, CategoriesResponse.class);
    }

    @Override
    public Category mapRequestToEntity(CategoriesRequest categoriesRequest) {
        return mapper.map(categoriesRequest, Category.class);
    }

    @Override
    public CategoriesResponse create(CategoriesRequest request) {
        return mapEntityToResponse(categoriesRepository.save(mapRequestToEntity(request)));
    }

    @Override
    public CategoriesResponse update(Long id, CategoriesRequest request) {
        return mapEntityToResponse(categoriesRepository.save(getById(id).setName(request.getName())));
    }

    @Override
    public CategoriesResponse delete(Long id) {
        CategoriesResponse byId = getResponseById(id);
        categoriesRepository.deleteById(id);
        return byId;
    }
}
