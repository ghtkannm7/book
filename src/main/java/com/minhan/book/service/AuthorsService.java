package com.minhan.book.service;


import com.minhan.book.base.BaseService;
import com.minhan.book.model.entity.Author;
import com.minhan.book.model.request.AuthorsRequest;
import com.minhan.book.model.response.AuthorsResponse;

public interface AuthorsService extends BaseService<Author, AuthorsRequest, AuthorsResponse, Long> {
}
