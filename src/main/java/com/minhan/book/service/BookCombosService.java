package com.minhan.book.service;

import com.minhan.book.base.BaseService;
import com.minhan.book.model.entity.BookCombos;
import com.minhan.book.model.request.BookCombosRequest;
import com.minhan.book.model.response.BookCombosResponse;

public interface BookCombosService extends BaseService<BookCombos, BookCombosRequest, BookCombosResponse, Long> {
    void subQuantity(Long bookCombosId, Integer quantityCI);
}
