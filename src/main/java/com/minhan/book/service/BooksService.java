package com.minhan.book.service;

import com.minhan.book.base.BaseService;
import com.minhan.book.model.entity.Book;
import com.minhan.book.model.request.BooksRequest;
import com.minhan.book.model.response.BooksResponse;

public interface BooksService extends BaseService<Book, BooksRequest, BooksResponse, Long> {
    void subQuantity(Long bookId, Integer quantityCI);
}
