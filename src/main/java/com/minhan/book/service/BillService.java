package com.minhan.book.service;

import com.minhan.book.base.BaseService;
import com.minhan.book.model.entity.Bill;
import com.minhan.book.model.request.BillRequest;
import com.minhan.book.model.response.BillResponse;

public interface BillService extends BaseService<Bill, BillRequest, BillResponse, Long> {
    BillResponse payAfterReceive(Long userId);

    BillResponse paymentByCard(Long userId);
}
