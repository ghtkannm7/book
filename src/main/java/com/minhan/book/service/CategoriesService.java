package com.minhan.book.service;

import com.minhan.book.base.BaseService;
import com.minhan.book.model.entity.Category;
import com.minhan.book.model.request.CategoriesRequest;
import com.minhan.book.model.response.CategoriesResponse;

public interface CategoriesService extends BaseService<Category, CategoriesRequest, CategoriesResponse, Long> {
}
