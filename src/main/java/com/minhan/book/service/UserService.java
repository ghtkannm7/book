package com.minhan.book.service;

import com.minhan.book.model.entity.User;
import com.minhan.book.model.request.UserRequest;
import com.minhan.book.model.response.UserResponse;
import com.minhan.book.base.BaseService;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService, BaseService<User, UserRequest, UserResponse, Long> {
    User getUserByMail(String mail);

}
