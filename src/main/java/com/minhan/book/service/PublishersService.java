package com.minhan.book.service;

import com.minhan.book.base.BaseService;
import com.minhan.book.model.entity.Publisher;
import com.minhan.book.model.request.PublishersRequest;
import com.minhan.book.model.response.PublishersResponse;

public interface PublishersService extends BaseService<Publisher, PublishersRequest, PublishersResponse, Long> {
}
