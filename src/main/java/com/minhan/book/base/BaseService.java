package com.minhan.book.base;

import com.minhan.book.model.error.ResponseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;

public interface BaseService<Entity, Request, Response, ID> {
    default Entity getById(ID id) {
        String type = ((Class<?>) getClass().getGenericInterfaces()[0]).getSimpleName().replace("Service", "");
        return getRepository().findById(id).orElseThrow(() -> new ResponseException(String.format("%s not found", type)));
    }

    default List<Entity> getByIds(Collection<ID> ids) {
        return getRepository().findByIdIn(ids);
    }

    default Page<Response> getPage(int page, int size, Sort sort) {
        return getRepository().findAll(PageRequest.of(page, size, sort)).map(this::mapEntityToResponse);
    }

    default Response getResponseById(ID id) {
        return mapEntityToResponse(getById(id));
    }

    default List<Response> getResponseByIds(Collection<ID> ids) {
        return getByIds(ids).stream().map(this::mapEntityToResponse).toList();
    }

    BaseRepository<Entity, ID> getRepository();

    Response mapEntityToResponse(Entity entity);

    Entity mapRequestToEntity(Request request);

    Response create(Request request);

    Response update(ID id, Request request);

    Response delete(ID id);
}
