package com.minhan.book.base;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

public interface BaseController<Request, Response, ID> {

    @GetMapping
    default ResponseEntity<Page<Response>> getPage(@RequestParam(defaultValue = "0") int page,
                                                   @RequestParam(defaultValue = "10") int size,
                                                   @RequestParam(defaultValue = "id") String sortBy,
                                                   @RequestParam(defaultValue = "ASC") Sort.Direction sortDirection) {
        return ResponseEntity.ok(getService().getPage(page, size, Sort.by(sortDirection, sortBy)));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    default ResponseEntity<Response> create(@RequestBody @Valid Request request) {
        return ResponseEntity.ok(getService().create(request));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    default ResponseEntity<Response> update(@PathVariable ID id, @RequestBody @Valid Request request) {
        return ResponseEntity.ok(getService().update(id, request));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    default ResponseEntity<Response> delete(@PathVariable ID id) {
        return ResponseEntity.ok(getService().delete(id));
    }

    BaseService<?, Request, Response, ID> getService();
}
