package com.minhan.book.model.error;

import io.jsonwebtoken.JwtException;

public class CustomJWTException extends JwtException {
    public CustomJWTException(String message) {
        super(message);
    }
}
