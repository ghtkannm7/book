package com.minhan.book.model.error;

public class ResponseException extends RuntimeException {
    public ResponseException(String message) {
        super(message);
    }
}
