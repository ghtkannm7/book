package com.minhan.book.model.error;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorMessage {
    private int status;
    private String error;
    private String message;
    private String path;
}
