package com.minhan.book.model.entity;

import com.minhan.book.model.constant.BookFormat;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

@Entity
@Table
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@SQLDelete(sql = "UPDATE books SET is_deleted = true WHERE id = ?")
@Where(clause = "is_deleted = false")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String title;

    private Double price;

    @Enumerated(EnumType.STRING)
    private BookFormat bookFormat;

    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "book_author",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private List<Author> authors;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;

    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "book_category",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private List<Category> categories;

    @ToString.Exclude
    @OneToMany(mappedBy = "book")
    private List<CartItems> cartItems;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "book_combos_id")
    private BookCombos bookCombos;

    @Builder.Default
    private Integer quantity = 1;

    @Column(columnDefinition = "BOOLEAN DEFAULT false")
    @Builder.Default
    private Boolean isDeleted = false;
}
