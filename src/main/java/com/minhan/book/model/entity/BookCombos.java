package com.minhan.book.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

@Entity
@Table
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@SQLDelete(sql = "UPDATE book_combos SET is_deleted = true WHERE id = ?")
@Where(clause = "is_deleted = false")
public class BookCombos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;

    private Double price;

    @Builder.Default
    private Integer quantity = 1;

    @Column(unique = true)
    @OneToMany(mappedBy = "bookCombos")
    private List<Book> books;

    @OneToMany(mappedBy = "bookCombos")
    private List<CartItems> cartItems;

    @Column(columnDefinition = "BOOLEAN DEFAULT false")
    @Builder.Default
    private Boolean isDeleted = false;
}
