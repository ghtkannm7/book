package com.minhan.book.model.entity;

import com.minhan.book.model.constant.BillStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.Date;

@Entity
@Table
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@SQLDelete(sql = "UPDATE bill SET is_deleted = true WHERE id = ?")
@Where(clause = "is_deleted = false")
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "cart_id")
    private Cart cart;

    private Double totalPrice;

    private Date orderDate;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private BillStatus status = BillStatus.WAIT;

    @Column(columnDefinition = "BOOLEAN DEFAULT false")
    @Builder.Default
    private Boolean isDeleted = false;
}
