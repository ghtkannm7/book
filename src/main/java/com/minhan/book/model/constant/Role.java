package com.minhan.book.model.constant;

public enum Role {
    USER, ADMIN
}
