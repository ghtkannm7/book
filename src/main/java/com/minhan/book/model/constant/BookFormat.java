package com.minhan.book.model.constant;

public enum BookFormat {
    EBOOK, PRINT, BOTH
}
