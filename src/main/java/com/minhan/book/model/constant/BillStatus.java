package com.minhan.book.model.constant;

public enum BillStatus {
    WAIT,
    CONFIRM,
    DELIVERY,
    RECEIVED
}
