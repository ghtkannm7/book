package com.minhan.book.model.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CartRequest {
    @NotNull(message = "User is required")
    private Long userId;

    @NotNull(message = "Cart item is required")
    private CartItemsRequest cartItemId;
}