package com.minhan.book.model.request;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.util.List;

@Data
public class BookCombosRequest {
    @NotBlank(message = "Name is required")
    private String name;

    @Min(value = 0, message = "Price must be greater than or equal to 0")
    private Double price;

    @Min(value = 0, message = "Quantity must be greater than or equal to 0")
    private Integer quantity = 1;

    @NotEmpty(message = "Books list cannot be empty")
    private List<Long> bookIds;
}
