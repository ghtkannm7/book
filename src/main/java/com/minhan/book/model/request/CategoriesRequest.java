package com.minhan.book.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CategoriesRequest {
    @NotBlank(message = "Name is required")
    private String name;
}
