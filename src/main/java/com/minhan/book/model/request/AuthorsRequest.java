package com.minhan.book.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AuthorsRequest {
    @NotBlank(message = "Name is required")
    private String name;
}
