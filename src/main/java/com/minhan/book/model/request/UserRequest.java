package com.minhan.book.model.request;

import com.minhan.book.model.constant.Role;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {
    @Email
    private String mail;

    @NotBlank
    private String password;

    @NotBlank
    private String phone;

    @Builder.Default
    private Role role = Role.USER;
}
