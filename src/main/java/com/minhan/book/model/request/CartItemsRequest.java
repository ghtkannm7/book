package com.minhan.book.model.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CartItemsRequest {
    private Long bookId;

    private Long bookComboId;

    @NotNull(message = "Quantity is required")
    private Integer quantity;
}
