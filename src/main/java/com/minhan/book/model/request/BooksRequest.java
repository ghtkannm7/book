package com.minhan.book.model.request;

import com.minhan.book.model.constant.BookFormat;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class BooksRequest {
    @NotBlank(message = "Title is required")
    private String title;

    @Min(value = 0, message = "Price must be greater than or equal to 0")
    private Double price;

    @Min(value = 0, message = "Quantity must be greater than or equal to 0")
    private Integer quantity = 1;

    @NotNull(message = "Book format is required")
    private BookFormat bookFormat;

    @NotNull(message = "Publisher ID is required")
    private Long publisherId;

    @NotEmpty(message = "Authors list cannot be empty")
    private List<Long> authorIds;

    @NotEmpty(message = "Categories list cannot be empty")
    private List<Long> categoryIds;
}
