package com.minhan.book.model.request;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BillRequest {
    @NotNull
    private Long cartId;

    @Min(value = 0, message = "Price must be greater than or equal to 0")
    private Double totalPrice;
}