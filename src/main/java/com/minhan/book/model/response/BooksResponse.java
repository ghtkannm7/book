package com.minhan.book.model.response;

import com.minhan.book.model.constant.BookFormat;
import lombok.Data;

import java.util.List;

@Data
public class BooksResponse {
    private Long id;
    private String title;
    private Double price;
    private Integer quantity;
    private BookFormat bookFormat;
    private PublishersResponse publisher;
    private List<AuthorsResponse> authors;
    private List<CategoriesResponse> categories;
}
