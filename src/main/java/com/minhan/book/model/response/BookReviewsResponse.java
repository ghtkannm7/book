package com.minhan.book.model.response;

import lombok.Data;

import java.util.Date;

@Data
public class BookReviewsResponse {
    private Long id;
    private String comment;
    private Double rating;
    private Date reviewDate;
    private Long bookId;
    private Long userId;
}
