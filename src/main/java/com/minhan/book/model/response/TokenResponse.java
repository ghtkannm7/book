package com.minhan.book.model.response;

import lombok.Data;

@Data
public class TokenResponse {
    private Long id;
    private Boolean expired;
    private Boolean revoked;
    private String token;
    private String tokenType;
    private Long userId;
}
