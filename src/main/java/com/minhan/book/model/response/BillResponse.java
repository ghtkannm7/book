package com.minhan.book.model.response;

import com.minhan.book.model.constant.BillStatus;
import lombok.Data;

import java.util.Date;

@Data
public class BillResponse {
    private Long id;
    private CartResponse cart;
    private Double totalPrice;
    private Date orderDate;
    private BillStatus status;
}
