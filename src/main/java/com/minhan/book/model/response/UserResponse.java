package com.minhan.book.model.response;

import com.minhan.book.model.constant.Role;
import lombok.Data;

@Data
public class UserResponse {
    private Long id;
    private String mail;
    private String phone;
    private Role role;
}
