package com.minhan.book.model.response;

import lombok.Data;

import java.util.List;

@Data
public class CartResponse {
    private Long id;
    private UserResponse user;
    private Double price;
    private List<CartItemsResponse> cartItems;
    private Boolean isCheckout;
}
