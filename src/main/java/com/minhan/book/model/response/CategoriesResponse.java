package com.minhan.book.model.response;

import lombok.Data;

@Data
public class CategoriesResponse {
    private Long id;
    private String name;
}
