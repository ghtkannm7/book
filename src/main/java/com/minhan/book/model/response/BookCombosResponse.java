package com.minhan.book.model.response;

import lombok.Data;

import java.util.List;

@Data
public class BookCombosResponse {
    private Long id;
    private String name;
    private Double price;
    private Integer quantity;
    private List<BooksResponse> books;
}
