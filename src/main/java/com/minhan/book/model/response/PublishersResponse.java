package com.minhan.book.model.response;

import lombok.Data;

@Data
public class PublishersResponse {
    private Long id;
    private String name;
}
