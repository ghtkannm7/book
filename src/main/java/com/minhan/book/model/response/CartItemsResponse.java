package com.minhan.book.model.response;

import lombok.Data;

@Data
public class CartItemsResponse {
    private Long id;
    private Integer quantity;
    private BooksResponse book;
    private BookCombosResponse bookCombos;
}
