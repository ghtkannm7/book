package com.minhan.book;

import com.minhan.book.service.AuthorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BookApplication implements CommandLineRunner {

    @Autowired
    private AuthorsService authorsService;

    public static void main(String[] args) {
        SpringApplication.run(BookApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        AuthorsResponse responseById = authorsService.getResponseById(1L);
//        System.out.println(responseById);
    }
}
