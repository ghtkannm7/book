package com.minhan.book.controller;

import com.minhan.book.base.BaseController;
import com.minhan.book.model.request.PublishersRequest;
import com.minhan.book.model.response.PublishersResponse;
import com.minhan.book.service.PublishersService;
import com.minhan.book.base.BaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publishers")
@RequiredArgsConstructor
public class PublishersController implements BaseController<PublishersRequest, PublishersResponse, Long> {
    private final PublishersService publisherService;

    @Override
    public BaseService<?, PublishersRequest, PublishersResponse, Long> getService() {
        return publisherService;
    }
}
