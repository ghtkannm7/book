package com.minhan.book.controller;

import com.minhan.book.base.BaseController;
import com.minhan.book.model.request.AuthorsRequest;
import com.minhan.book.model.response.AuthorsResponse;
import com.minhan.book.service.AuthorsService;
import com.minhan.book.base.BaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorsController implements BaseController<AuthorsRequest, AuthorsResponse, Long> {
    private final AuthorsService authorsService;

    @Override
    public BaseService<?, AuthorsRequest, AuthorsResponse, Long> getService() {
        return authorsService;
    }
}
