package com.minhan.book.controller;

import com.minhan.book.base.BaseController;
import com.minhan.book.base.BaseService;
import com.minhan.book.model.request.BookCombosRequest;
import com.minhan.book.model.response.BookCombosResponse;
import com.minhan.book.service.BookCombosService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/book-combos")
@RequiredArgsConstructor
public class BookCombosController implements BaseController<BookCombosRequest, BookCombosResponse, Long> {
    private final BookCombosService bookCombosService;

    @Override
    public BaseService<?, BookCombosRequest, BookCombosResponse, Long> getService() {
        return bookCombosService;
    }
}
