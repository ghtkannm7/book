package com.minhan.book.controller;

import com.minhan.book.base.BaseController;
import com.minhan.book.base.BaseService;
import com.minhan.book.model.request.CartRequest;
import com.minhan.book.model.response.CartItemsResponse;
import com.minhan.book.model.response.CartResponse;
import com.minhan.book.service.CartService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cart")
@RequiredArgsConstructor
public class CartController implements BaseController<CartRequest, CartResponse, Long> {
    private final CartService cartService;

    @Override
    public BaseService<?, CartRequest, CartResponse, Long> getService() {
        return cartService;
    }

    @GetMapping("/unpaid/{id}")
    public ResponseEntity<CartResponse> getUnpaidCart(@PathVariable Long id) {
        return ResponseEntity.ok(cartService.getUserUnpaidCarts(id));
    }

    @PostMapping("/add")
    public ResponseEntity<CartResponse> addToCart(@RequestBody @Valid CartRequest cartRequest) {
        return ResponseEntity.ok(cartService.addToCart(cartRequest));
    }

    @DeleteMapping("/delete-item")
    public ResponseEntity<CartItemsResponse> deleteCartItem(@RequestParam Long cartId, @RequestParam Long cartItemId) {
        return ResponseEntity.ok(cartService.deleteCartItem(cartId, cartItemId));
    }
}
