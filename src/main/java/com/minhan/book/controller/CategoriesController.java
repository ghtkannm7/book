package com.minhan.book.controller;

import com.minhan.book.base.BaseController;
import com.minhan.book.model.request.CategoriesRequest;
import com.minhan.book.model.response.CategoriesResponse;
import com.minhan.book.service.CategoriesService;
import com.minhan.book.base.BaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoriesController implements BaseController<CategoriesRequest, CategoriesResponse, Long> {
    private final CategoriesService categoriesService;

    @Override
    public BaseService<?, CategoriesRequest, CategoriesResponse, Long> getService() {
        return categoriesService;
    }
}
