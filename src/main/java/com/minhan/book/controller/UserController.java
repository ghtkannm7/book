package com.minhan.book.controller;

import com.minhan.book.base.BaseController;
import com.minhan.book.model.request.UserRequest;
import com.minhan.book.model.response.UserResponse;
import com.minhan.book.service.UserService;
import com.minhan.book.base.BaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController implements BaseController<UserRequest, UserResponse, Long> {
    private final UserService userService;

    @Override
    public BaseService<?, UserRequest, UserResponse, Long> getService() {
        return userService;
    }
}
