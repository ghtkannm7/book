package com.minhan.book.controller;

import com.minhan.book.base.BaseController;
import com.minhan.book.base.BaseService;
import com.minhan.book.model.request.BillRequest;
import com.minhan.book.model.response.BillResponse;
import com.minhan.book.service.BillService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bill")
@RequiredArgsConstructor
public class BillController implements BaseController<BillRequest, BillResponse, Long> {
    private final BillService billService;

    @Override
    public BaseService<?, BillRequest, BillResponse, Long> getService() {
        return billService;
    }

    @PostMapping("/checkout")
    public ResponseEntity<BillResponse> payAfterReceive(@RequestParam Long userId) {
        return ResponseEntity.ok(billService.payAfterReceive(userId));
    }

    @PostMapping("/checkout-card")
    public ResponseEntity<BillResponse> paymentByCard(@RequestParam Long userId) {
        return ResponseEntity.ok(billService.paymentByCard(userId));
    }
}
