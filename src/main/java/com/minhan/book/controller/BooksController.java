package com.minhan.book.controller;

import com.minhan.book.base.BaseController;
import com.minhan.book.model.request.BooksRequest;
import com.minhan.book.model.response.BooksResponse;
import com.minhan.book.service.BooksService;
import com.minhan.book.base.BaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BooksController implements BaseController<BooksRequest, BooksResponse, Long> {
    private final BooksService booksService;

    @Override
    public BaseService<?, BooksRequest, BooksResponse, Long> getService() {
        return booksService;
    }
}
