# Buid
FROM maven:3.8.4-openjdk-17-slim AS build
WORKDIR /app
COPY . /app/book-application
RUN mvn clean package -f /app/book-application/pom.xml
RUN mvn package -f /app/book-application/pom.xml

# Multi-staging
FROM openjdk:17-slim
WORKDIR /app
COPY --from=build /app/book-application/target/Book-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]